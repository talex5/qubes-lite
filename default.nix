{ pkgs, crosvm, wayland-proxy-virtwl }: with pkgs;

let
  # Kernel to run in guest VMs
  linuxPackages = pkgs.linuxPackages_6_6;
  linux_vm = linuxPackages.kernel;

  python3Packages = pkgs.python3Packages;
  borb =
    python3Packages.buildPythonPackage rec {
      pname = "borb";
      version = "2.1.13";
      src = python3Packages.fetchPypi {
        inherit pname version;
        sha256 = "sha256-wH4ZB3qJoPsCE/bZO9csmz4Xt00S1VB3Jnv4/3C/4lI=";
      };
      doCheck = false;
      propagatedBuildInputs = with python3Packages; [
      # Specify dependencies
      lxml qrcode fonttools python-barcode cryptography requests pillow
    ];
  };

  my-python-packages =
    python-packages: with python-packages; [
      python3Packages.requests
      python3Packages.requests_oauthlib
      python3Packages.dateutil
      borb
    ];
  my-python = python3.withPackages my-python-packages;

  # Programs to add to the PATH in guests, as symlinks to /nix/store,
  # which is mounted from the host.
  userenv = pkgs.buildEnv {
    name = "vm-userenv";
    paths = [
      wayland-proxy-virtwl
      firefox
      evince
      libreoffice
      thunderbird
      shotwell
      hledger
      gcc
      strace
      gnumake
      git
      gdb
      xwayland
      xfce.xfce4-terminal
      wrk
      gargoyle
      rox-filer
      diffpdf
      linuxPackages.perf
      util-linux
      my-python
    ];
  };

  # A root filesystem for most of the guests.
  # Based on spectrum demo.
  rootfs = callPackage (import ./rootfs) {
    linuxModules = linux_vm;
    inherit (skawarePackages) s6-linux-init;
    doas = doas.override { withPAM = false; };
    dbusServices = [
      xfce.xfconf
      dconf       # Needed by shotwell
    ];
  };

  # Modules to put in the initrd for guests.
  # At the least, we need to load virtiofs here in order to mount /nix/store from the host.
  # This avoids the need to compile a custom kernel, which takes ages.
  modulesClosure = makeModulesClosure {
    kernel = linux_vm;
    rootModules = [
      "virtio"
      "virtio_net"
      "virtio_pci"
      "virtio_mmio"
      "virtio_blk"
      "virtiofs"
      "virtio_balloon"
      "virtio_rng"
      "virtio_console"
      "ext4"
      "squashfs"
      "nf_nat"
      "nft_nat"
      "nf_conntrack"
      "nf_conntrack_bridge"
      "btrfs"
      "overlay"

      # Needed for Docker with Linux 5.16
      "nf_tables"
      "xt_conntrack"
      "nft_compat"
      "xt_MASQUERADE"
      "nft_chain_nat"
      "xt_addrtype"
      "veth"

      "agpgart"
      "drm"
      "virtio_gpu"

      "vsock"
      "vmw_vsock_virtio_transport"

      "fuse"

      "virtio-rng"
    ];
    firmware = linux_vm;
  };

  # Initial boot script that runs from the initrd.
  # Load the bundled modules, mount the root filesystem, and run the init process from there.
  # Not sure the setserial stuff actually does anything.
  # The problem is that if crosvm doesn't respond to Linux's probe quickly enough
  # then Linux falls back to a very slow polling mode for the console.
  # Based on nixpkgs/pkgs/build-support/vm/default.nix
  stage1Init = writeScript "vm-run-stage1" ''
    #! ${vmTools.initrdUtils}/bin/ash -e
    export PATH=${vmTools.initrdUtils}/bin
    echo "initrd boot script running!"
    echo "loading kernel modules..."
    for i in $(cat ${modulesClosure}/insmod-list); do
      #echo $i
      ${kmod}/bin/insmod $i || echo "warning: unable to load $i"
    done
    mkdir /fs
    #sleep 1
    mount -t devtmpfs devtmpfs /dev
    ${setserial}/bin/setserial /dev/ttyS0 irq 4 || sleep 1
    ${setserial}/bin/setserial /dev/ttyS1 irq 3 || sleep 1
    ${setserial}/bin/setserial /dev/ttyS2 irq 4 || sleep 1
    ${setserial}/bin/setserial /dev/ttyS3 irq 3 || sleep 1
    #ls /dev
    #sleep 1
    if [ -b /dev/vda1 ]; then
      mount -t ext4 /dev/vda1 /fs
    else
      mount -t squashfs /dev/vda /fs
    fi
    mount -o bind /dev /fs/dev
    #echo switch_root
    #sleep 1
    exec switch_root /fs /sbin/init
  '';

  initrd = makeInitrd {
    contents = [
      { object = stage1Init;
        symlink = "/init";
      }
    ];
  };

  mktuntap = callPackage (import ./mktuntap) {};

  # Create a vs* script to launch a VM.
  mkStart = { name, command ? "", cpus ? 2, network ? true, hostNumber, rootDev, disks, memory ? 4096 }:
    { inherit name;
      value =
        ''
          #!${execline}/bin/execlineb -s0
          foreground { umount /run/user/1000/doc }
          backtick command { pipeline { echo -n $@ } base64 -w0 }
          importas -iu command command
          importas -iu XDG_RUNTIME_DIR XDG_RUNTIME_DIR
          importas -iu WAYLAND_DISPLAY WAYLAND_DISPLAY
        '' + lib.optionalString network ''
          ${mktuntap}/bin/mktuntap -i tap${name} -pvB 3
        '' + ''
          ${pkgs.s6}/bin/s6-softlimit -l 0
          "${lib.getBin crosvm}/bin/crosvm" run
          -m ${toString memory}
          --initrd=${initrd}/initrd
          --serial=hardware=virtio-console
          ${builtins.concatStringsSep "\n" disks}
          --shared-dir "/home/tal/vms/shared/${name}:mtdshared:type=fs"
          --shared-dir "/nix:mtdnix:type=fs"
          --shared-dir "${linux_vm}/lib/modules:mtdmodules:type=fs"
          --shared-dir "${linux_vm.dev}/lib/modules:mtdmodulesdev:type=fs"
          -p "userenv=${userenv}"
          -p "spectrumcmd=$command"
          -p "spectrumname=${name}"
        '' +
        lib.optionalString network (
          let
            ip   = "10.0.0.${toString hostNumber}";
            gw   = "10.0.0.${toString (hostNumber - 1)}";
            ipv6 = "fd4d:06ff:48e4:${toString (hostNumber - 1)}::2/48";
            gwv6 = "fd4d:06ff:48e4:${toString (hostNumber - 1)}::1";
          in
          ''
            -p "spectrumip=${ip}"
            -p "spectrumgw=${gw}"
            -p "spectrumip6=${ipv6}"
            -p "spectrumgw6=${gwv6}"
            --tap-fd 3
          ''
        ) + ''
          --cid ${toString hostNumber}
          -p root=${rootDev}
          --seccomp-log-failures
          --cpus ${toString cpus}
          --gpu=context-types=cross-domain:virgl2
          -s $\{XDG_RUNTIME_DIR}"/crosvm-${name}.sock"
          --wayland-sock "$\{XDG_RUNTIME_DIR}/$\{WAYLAND_DISPLAY}"
          "${linux_vm}/bzImage"
        '';
      };

    # Configuration for each VM.
    vms = [
      {
        name = "dev";
        memory = 12288;
        rootDev = "/dev/vda1";
        hostNumber = 3;
        cpus = 8;
        disks = [
          "--rwdisk /home/tal/vms/debian-root.qcow"
          "--rwdisk /dev/mapper/main-dev"
          "--rwdisk /dev/mapper/main-docker"
        ];
      }
      {
        name = "com";
        memory = 6144;
        rootDev = "/dev/vda";
        hostNumber = 5;
        disks = [
          "--disk ${rootfs.squashfs}"
          "--rwdisk /dev/mapper/main-com"
        ];
      }
      {
        name = "shopping";
        memory = 4096;
        rootDev = "/dev/vda";
        hostNumber = 7;
        disks = [
          "--disk ${rootfs.squashfs}"
          "--rwdisk /dev/mapper/main-shopping"
        ];
      }
      {
        name = "banking";
        memory = 4096;
        rootDev = "/dev/vda";
        hostNumber = 9;
        disks = [
          "--disk ${rootfs.squashfs}"
          "--rwdisk /dev/mapper/main-banking"
        ];
      }
      {
        name = "untrusted";
        memory = 4096;
        rootDev = "/dev/vda";
        hostNumber = 11;
        disks = [
          "--disk ${rootfs.squashfs}"
          "--rwdisk /dev/mapper/main-untrusted"
        ];
      }
      {
        name = "personal";
        memory = 4096;
        network = false;
        hostNumber = 13;
        rootDev = "/dev/vda";
        disks = [
          "--disk ${rootfs.squashfs}"
          "--rwdisk /dev/mapper/main-personal"
        ];
      }
    ];

  # The list of vs* scripts.
  vmScripts = map mkStart vms;

  attrs = builtins.listToAttrs (map ({name, value}: { name = "${name}Script"; inherit value; }) vmScripts);

  # Systemd unit file to also run the proxy on the host. This fixes some DPI problems with Xwayland.
  xwaylandService = ''
    [Unit]
    Description=Xwayland proxy

    [Service]
    Environment=WAYLAND_DISPLAY=wayland-1
    ExecStart=${wayland-proxy-virtwl}/bin/wayland-proxy-virtwl --tag="[host] " --wayland-display xwayland-proxy --x-display=0 --xwayland-binary=${xwayland}/bin/Xwayland --xrdb Xft.dpi:150 -v --log-suppress motion --log-ring-path /home/tal/wayland.log --x-unscale=2

    [Install]
    WantedBy=default.target
  '';

  # Scripts to launch Firefox and xfce4-terminal in a VM, directly from the host.
  spawnApp = { name, hostNumber, ... } :
  if name == "dev" then
    ''
    cat > "$out/bin/tt${name}" << 'EOF'
    #!/bin/sh
    ssh -A user@10.0.0.3 env DISPLAY=:0 WAYLAND_DISPLAY=wayland-0 ${pkgs.xfce.xfce4-terminal}/bin/xfce4-terminal "$@" &
    EOF
    cat > "$out/bin/ff${name}" << 'EOF'
    #!/bin/sh
    args=(); for v in "$@"; do args+=("$(printf %q "$v")"); done
    ssh user@10.0.0.3 env WAYLAND_DISPLAY=wayland-0 /home/user/bin/firefox "${"$"}{args[@]}" &
    EOF
    ''
  else
    ''
    cat > "$out/bin/tt${name}" << EOF
    #!/bin/sh
    echo 'xfce4-terminal&' | ${pkgs.socat}/bin/socat stdin vsock-connect:${toString hostNumber}:5000
    EOF
    cat > "$out/bin/ff${name}" << EOF
    #!/bin/sh
    echo 'firefox 2>/dev/null &' | ${pkgs.socat}/bin/socat stdin vsock-connect:${toString hostNumber}:5000
    EOF
    '';
in
  runCommand "qubes-lite" (attrs // { inherit xwaylandService; })
  ''
    mkdir -p "$out/bin"
    ${builtins.concatStringsSep "\n" (map ({name, value}: ''echo -n "$'' + ''${name}Script" > "$out/bin/vs${name}"'') vmScripts)}
    ${builtins.concatStringsSep "\n" (map spawnApp vms)}
    chmod +x "$out/bin/"*
    ln -s "${wayland-proxy-virtwl}/bin/wayland-proxy-virtwl" "$out/bin/wayland-proxy-virtwl"
    mkdir -p "$out/share/systemd/user"
    echo -n "$xwaylandService" > "$out/share/systemd/user/xwayland-proxy.service"
    ln -s ${rootfs} "$out/rootfs-gc-keep"
    ln -s ${crosvm}/bin/crosvm "$out/bin/crosvm"
  ''
