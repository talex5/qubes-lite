{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    wayland-proxy-virtwl = {
      url = "git+https://github.com/talex5/wayland-proxy-virtwl.git?submodules=1";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    crosvm = {
      url = "git+https://gitlab.com/talex5/crosvm.git?ref=b124-tal&submodules=1";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, wayland-proxy-virtwl, crosvm }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages.default = import ./. {
          inherit pkgs;
          wayland-proxy-virtwl = wayland-proxy-virtwl.packages.${system}.default;
          crosvm = crosvm.packages.${system}.default;
        };
      }
    );
}
