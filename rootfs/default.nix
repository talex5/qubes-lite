{ runCommandNoCC, writeScript, writeText, makeFontsConf, writeClosure
, lib, dash, busybox, execline, s6, s6-portable-utils, s6-linux-utils
, s6-linux-init, mesa, squashfs-tools-ng, dbus, makeDBusConf, dbusServices
, source-code-pro
, pciutils, bashInteractive, fontconfig, linuxModules, utillinux, vim_configurable
, fish, xdpyinfo, xwininfo, xprop, xterm
, glibcLocales, cacert, doas, socat, tzdata
}:

let
  makeRootfs = import ./generic.nix {
    inherit runCommandNoCC writeScript writeClosure makeFontsConf lib dbus makeDBusConf dbusServices
      dash execline s6 s6-portable-utils s6-linux-utils s6-linux-init busybox tzdata
      mesa squashfs-tools-ng bashInteractive fish linuxModules vim_configurable cacert doas;
  };

  path = [
    dash fish pciutils busybox s6 execline fontconfig.bin utillinux vim_configurable socat xdpyinfo xwininfo xprop xterm
  ];

  login = writeScript "login" ''
    #! ${execline}/bin/execlineb -s0
    unexport !
    ${busybox}/bin/login -p -f root $@
  '';
in

makeRootfs {
  services.getty.run = writeScript "getty-run" ''
    #! ${execline}/bin/execlineb -P
    export TERM xterm
    export XDG_RUNTIME_DIR /run/user/0
    export FONTCONFIG_FILE /etc/fonts/fonts.conf
    export PATH /bin:/home/user/.nix-profile/bin/:${lib.makeBinPath path}
    export PS1 "\\h:\\w# "
    ${busybox}/bin/getty -i -n -l ${login} 38400 ttyS0
  '';

  run = ''
    if { chown user /dev/dri/card0 }
    if { chmod a+rw /dev/vsock }

    s6-applyuidgid -u 1000 -g 1000
    export LOCALE_ARCHIVE ${glibcLocales}/lib/locale/locale-archive
    export LANG en_GB.UTF-8
    export XDG_RUNTIME_DIR /run/user/1000
    export FONTCONFIG_FILE /etc/fonts/fonts.conf
    export PATH /home/user/.nix-profile/bin/:${lib.makeBinPath path}:/bin
    export DISPLAY :0
    export MOZ_ENABLE_WAYLAND 1
    export HOME /home/user
    cd /home/user
    backtick -n vmname { hostname }
    importas -iu vmname vmname
    wayland-proxy-virtwl --virtio-gpu --tag="[$\{vmname}] " --x-display=0 --xrdb Xft.dpi:150 -v --log-suppress motion --log-ring-path /home/user/wayland.log --x-unscale=2 --
    ${dash}/bin/dash -c "(while true; do socat vsock-listen:5000 exec:dash; done)"
  '';
    #${westonLite}/bin/weston-terminal --shell ${bashInteractive}/bin/bash --font-size 32pt --font "Source Code Pro"
    #export WAYLAND_DEBUG 1
    #foreground { mount /dev/vdb /home }
    #${dash}/bin/dash -c "kitty ${fish}/bin/fish &"

  fonts = [ source-code-pro ];
}
