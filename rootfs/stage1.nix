{ writeScript, lib
, execline, s6, s6-portable-utils, s6-linux-utils, s6-linux-init, busybox, mesa
, bashInteractive
}:

{ run ? "true" }:

let
  path = [ s6 s6-portable-utils s6-linux-utils s6-linux-init busybox execline bashInteractive ];
in

writeScript "init-stage1" ''
  #! ${execline}/bin/execlineb -P
  export PATH ${lib.makeBinPath path}
  ${s6}/bin/s6-setsid -qb --

  importas -i spectrumcmd spectrumcmd
  importas -D- spectrumip spectrumip
  importas -D- spectrumip6 spectrumip6
  importas -D- spectrumgw spectrumgw
  importas -D- spectrumgw6 spectrumgw6
  importas -D- spectrumname spectrumname
  importas -D- userenv userenv

  umask 022
  if { s6-mount -t tmpfs -o mode=0755 tmpfs /run }
  if { s6-hiercopy /etc/service /run/service }
  emptyenv -p

  if { hostname $spectrumname }
  if { mount -t virtiofs mtdshared /shared }

  foreground {
    if { mount -t tmpfs tmpfs /tmp }
    mkdir --mode=1777 /tmp/.X11-unix
  }

  background {
    s6-setsid --
    if { s6-mkdir -p /run/user/0 /dev/pts /dev/shm }
    if { install -o user -g user -d /run/user/1000 }
    if { s6-mount -t devpts -o gid=4,mode=620 none /dev/pts }
    if { s6-mount -t tmpfs none /dev/shm }
    if { s6-mount -t proc none /proc }
    if { s6-mount -t sysfs none /sys }
    if { s6-mount -t ext4 /dev/vdb /home/user }

    if { s6-mount -t virtiofs mtdnix /nix }
    if { mkdir -p /run/current-system/sw }
    if { ln -s /bin /run/current-system/sw/bin }
    foreground { rm -f /home/user/.nix-profile }
    foreground { ln -s $userenv /home/user/.nix-profile }
    foreground { install -m u+s /bin/doas.unwrapped /tmp/doas }

    if { s6-ln -s ${mesa.drivers} /run/opengl-driver }

    if { ifconfig lo 127.0.0.1 up }

    ifthenelse { test $spectrumip = "-" }
      { }
      {
        if { ifconfig eth0 $spectrumip netmask 255.255.255.254 up }
        if { route add default gateway $spectrumgw }
        if { ifconfig eth0 $spectrumip6 up }
        route add -A inet6 default gw $spectrumgw6
      }

    export HOME /
    export XDG_RUNTIME_DIR /run/user/0
    foreground {
      ifelse { test -n $spectrumcmd }
        { pipeline { heredoc 0 $spectrumcmd base64 -d } /bin/sh }
        ${run}
    }
    importas -i ? ?
    if { s6-echo STATUS: $? }
    s6-svscanctl -t /run/service
  }

  unexport !
  cd /run/service
  s6-svscan
''
