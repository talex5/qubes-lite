{ runCommandNoCC, writeScript, writeClosure, makeFontsConf, lib, dbus, dbusServices, makeDBusConf
, dash, execline, s6, s6-portable-utils, s6-linux-utils, s6-linux-init, busybox, tzdata
, mesa, squashfs-tools-ng, bashInteractive, linuxModules, fish, vim_configurable, cacert, doas
}:

{ services, run, fonts ? [], path ? [] }:

let
  makeStage1 = import ./stage1.nix {
    inherit writeScript lib
      execline s6 s6-portable-utils s6-linux-utils s6-linux-init busybox mesa bashInteractive;
  };

  makeServicesDir = import ./services.nix {
    inherit runCommandNoCC writeScript lib execline;
  };

  fontsConf = makeFontsConf { fontDirectories = fonts; };

  squashfs = runCommandNoCC "root-squashfs" {} ''
    cd ${rootfs}
    (
        grep -v ^${rootfs} ${writeClosure [rootfs]}
        printf "%s\n" *
    ) \
        | xargs tar -cP --owner root:0 --group root:0 --hard-dereference \
        | ${squashfs-tools-ng}/bin/tar2sqfs -c gzip -X level=1 $out
  '';

  tzdir = "${tzdata}/share/zoneinfo";

  dbusConf = makeDBusConf {
    serviceDirectories = dbusServices;
    suidHelper = "no-dbus-daemon-launch-helper";
  };

  rootfs = runCommandNoCC "rootfs" { passthru = { inherit squashfs; }; } ''
    mkdir $out
    cd $out

    mkdir usr bin sbin dev sys proc run tmp home shared root
    mkdir usr/bin
    ln -s ${busybox}/bin/env usr/bin/
    ln -s ${dash}/bin/dash bin/sh
    ln -s ${vim_configurable}/bin/vim bin/vi
    ln -s ${bashInteractive}/bin/bash bin/bash
    ln -s ${fish}/bin/fish bin/fish
    ln -s ${dbus}/bin/dbus-launch bin/dbus-launch
    ln -s ${dbus}/bin/dbus-daemon bin/dbus-daemon
    install ${doas}/bin/doas bin/doas.unwrapped
    ln -s /tmp/doas bin/doas
    echo "alias vmhalt='reboot; exit'" > root/.bash_profile
    ln -s ${makeStage1 { inherit run; }} sbin/init
    ln -s ${linuxModules}/lib lib
    cp -r ${./etc} etc
    chmod u+w etc
    mkdir -p etc/ssl/certs
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt etc/ssl/certs/ca-bundle.crt
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt etc/ssl/certs/ca-certificates.crt
    ln -s ${tzdir} etc/zoneinfo
    ln -s zoneinfo/Europe/London etc/localtime
    ln -s ${dbusConf} etc/dbus-1

    mkdir etc/fonts
    ln -s ${fontsConf} etc/fonts/fonts.conf

    mkdir home/user

    touch etc/login.defs
    cp -r ${makeServicesDir { inherit services; }} etc/service
  '';
in
rootfs
